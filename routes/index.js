var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var url = require('../config/keys').MongoURI;

router.get('/', (req, res) => {
    res.render('home-page');
});

router.get('/dashboard', (req, res) => {
var list = []
MongoClient.connect(url, (err, db) => {
    var dbo = db.db('test');
    for(var i = 0; i < req.user.friends.length; i++){
    dbo.collection('users').findOne({email: req.user.friends[i]}, (err, result) => {
        if(err) throw err
        list[i] = result
        //console.log(result)
    })
    }
})
console.log(list)
    res.render('dashboard', {
        name: req.user.name,
        phone: req.user.phone,
        email: req.user.email,
        friends: list
    });

});



module.exports = router;